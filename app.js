const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const db = require("./config/database");

db.authenticate()
  .then(() => console.log("Database connected sucessfully..."))
  .catch((err) => console.log(err));

const app = express();

app.use("/api", require("./routes/apis"));

app.listen(process.env.PORT || 5001, () => {
  console.log("App up and running");
});
