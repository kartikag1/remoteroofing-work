const express = require("express");
const router = express.Router();
const db = require("../config/database");
const User = require("../models/User");
const Project = require("../models/Project");
const Task = require("../models/Task");

/*----------------------------------------API to create tables----------------------------------------*/

router.get("/createTables", (req, res) => {
  Project.sync()
    .then(function () {
      console.log("table created");
    })
    .catch(function (error) {
      console.log(err);
    });

  Task.sync()
    .then(function () {
      console.log("table created");
    })
    .catch(function (error) {
      console.log(err);
    });

  User.sync()
    .then(function () {
      console.log("table created");
    })
    .catch(function (error) {
      console.log(err);
    });
  res.send("created");
});

/*------------------------------------------API to retrieve------------------------------------------*/

//http://localhost:5001/api/users?page=1&limit=2&filter=on&name=*&surname=*&sortBy=name&order=ASC
router.get("/users", async (req, res) => {
  console.log(req.query.name);
  console.log(req.query.surname);
  let limit = req.query.limit;
  let offset = 0 + (req.query.page - 1) * limit;
  if (req.query.sortBy && req.query.order) {
    if (req.query.filter == "on" && req.query.name && req.query.surname) {
      await User.findAll({
        offset: offset,
        limit: limit,
        where: {
          name: req.query.name,
          surname: req.query.surname,
        },
        order: [[req.query.sortBy, req.query.order]],
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    } else if (req.query.filter == "on" && req.query.name) {
      await User.findAll({
        offset: offset,
        limit: limit,
        where: {
          name: req.query.name,
        },
        order: [[req.query.sortBy, req.query.order]],
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    } else if (req.query.filter == "on" && req.query.surname) {
      await User.findAll({
        offset: offset,
        limit: limit,
        where: {
          surname: req.query.surname,
        },
        order: [[req.query.sortBy, req.query.order]],
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    } else {
      await User.findAll({
        offset: offset,
        limit: limit,
        order: [[req.query.sortBy, req.query.order]],
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    }
  } else {
    if (req.query.filter == "on" && req.query.name && req.query.surname) {
      await User.findAll({
        offset: offset,
        limit: limit,
        where: {
          name: req.query.name,
          surname: req.query.surname,
        },
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    } else if (req.query.filter == "on" && req.query.name) {
      await User.findAll({
        offset: offset,
        limit: limit,
        where: {
          name: req.query.name,
        },
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    } else if (req.query.filter == "on" && req.query.surname) {
      await User.findAll({
        offset: offset,
        limit: limit,
        where: {
          surname: req.query.surname,
        },
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    } else {
      await User.findAll({
        offset: offset,
        limit: limit,
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => console.log(err));
    }
  }
});

router.get("/projects", (req, res) => {
  let limit = req.query.limit;
  let offset = 0 + (req.query.page - 1) * limit;
  Project.findAll({
    name: req.query.name,
    offset: offset,
    limit: limit,
    where: {
      name: JSON.parse(req.query.nameFilter),
      body: JSON.parse(req.query.bodyFilter),
      status: JSON.parse(req.query.statusFilter),
      assigner_name: JSON.parse(req.query.assignerNameFilter),
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => console.log(err));
});

router.get("/tasks", (req, res) => {
  let limit = req.query.limit;
  let offset = 0 + (req.query.page - 1) * limit;
  Task.findAll({
    offset: offset,
    limit: limit,
    where: {
      name: JSON.parse(req.query.nameFilter),
      description: JSON.parse(req.query.descFilter),
      status: JSON.parse(req.query.statusFilter),
      assigner_name: JSON.parse(req.query.assignerNameFilter),
      assignee_name: JSON.parse(req.query.assigneeNameFilter),
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => console.log(err));
});

/*-------------------------------------------APIs TO STORE DATA-------------------------------------------*/

router.post("/users", async (req, res) => {
  const data = {
    name: req.body.name,
    surname: req.body.surname,
    email: req.body.email,
  };
  await User.create(data)
    .then((dta) => res.send(dta))
    .catch((err) => res.send(err));
});

router.post("/projects", async (req, res) => {
  const data = {
    name: req.body.name,
    body: req.body.body,
    status: req.body.status,
    assigner_name: req.body.assigner_name,
  };
  await Project.create(data)
    .then((dta) => res.send(dta))
    .catch((err) => res.send(err));
});

router.post("/tasks", async (req, res) => {
  const data = {
    name: req.body.name,
    description: req.body.description,
    score: req.body.score,
    status: req.body.status,
    assigner_name: req.body.assigner_name,
    assignee_name: req.body.assignee_name,
    project_name: req.body.project_name,
  };
  await Task.create(data)
    .then((dta) => res.send(dta))
    .catch((err) => res.send(err));
});

module.exports = router;
