const Sequelize = require("sequelize");
const db = require("../config/database");

const Task = db.define("Tasks", {
  name: {
    type: Sequelize.STRING,
  },
  description: {
    type: Sequelize.TEXT,
  },
  score: {
    type: Sequelize.INTEGER,
  },
  status: {
    type: Sequelize.ENUM,
    values: ["active", "inactive", "declined", "completed"],
  },
  assigner_name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  assignee_name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  project_name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  createdAt: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    defaultValue: Sequelize.fn("now"),
  },
  updatedAt: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    defaultValue: Sequelize.fn("now"),
  },
});

module.exports = Task;
