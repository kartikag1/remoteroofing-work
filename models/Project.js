const Sequelize = require("sequelize");
const db = require("../config/database");

const Project = db.define("Projects", {
  name: {
    type: Sequelize.STRING,
  },
  body: {
    type: Sequelize.TEXT,
  },
  status: {
    type: Sequelize.ENUM,
    values: ["active", "inactive", "declined", "completed"],
  },
  assigner_name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  createdAt: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    defaultValue: Sequelize.fn("now"),
  },
  updatedAt: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    defaultValue: Sequelize.fn("now"),
  },
});

module.exports = Project;
