const Sequelize = require("sequelize");
const db = require("../config/database");

const User = db.define("Users", {
  name: {
    type: Sequelize.STRING,
  },
  surname: {
    type: Sequelize.STRING,
  },
  email: {
    type: Sequelize.STRING,
    primaryKey: true,
  },
  createdAt: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    defaultValue: Sequelize.fn("now"),
  },
  updatedAt: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    defaultValue: Sequelize.fn("now"),
  },
});

module.exports = User;
